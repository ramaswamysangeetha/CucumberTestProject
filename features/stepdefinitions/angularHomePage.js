"use strict";
const protractor_1 = require("protractor");
var { Given, When, Then, setDefaultTimeout } = require("cucumber");
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;
module.exports = function steps() {
    this.Given(/^I visit the Angular homepage$/, function () {
        return protractor_1.browser.getTitle();
    });
    this.Then(/^Clicking on the Learn Header menu element on the page$/, function () {
        this.setDefaultTimeout(60000);
        var header = protractor_1.element(protractor_1.by.xpath(".//*[@id='navbar-main']/div/div/ul/li[1]/a"));
        return header.click();
    });
    this.Then(/^Counting the dropDown list of learn menu Header$/, function () {
        var learnList = protractor_1.element.all(protractor_1.by.xpath("//a[text()='Learn']/../ul//li"));
        expect(learnList.count()).to.eventually.equal(5);
        learnList.count().then(function (size) {
            console.log(size);
        });
        for (var i = 0; i < learnList.length; i++) {
            protractor_1.element.all(protractor_1.by.xpath("//a[text()='Learn']/../ul//li//a")).get(i).getText().then(function (text) {
                console.log(text);
            });
        }
    });
    this.Then(/^Enterting text in name box of The Basics tab$/, function () {
        protractor_1.element(protractor_1.by.model('yourName')).sendKeys("RamaSwamy");
        return protractor_1.element(protractor_1.by.css('.well .ng-binding')).getText().then(function (text) {
            console.log(text);
        });
    });
};
