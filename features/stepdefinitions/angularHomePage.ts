import { browser, by, element } from "protractor";
import { homePage } from "../../pages/HomePage";
var { Given, When, Then, setDefaultTimeout} = require("cucumber");
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;

export = function steps() {

        this.Given(/^I visit the Angular homepage$/, function () {
                return browser.getTitle();

        });

        this.Then(/^Clicking on the Learn Header menu element on the page$/, function () {
                this.setDefaultTimeout(60000);
                var header = element(by.xpath(".//*[@id='navbar-main']/div/div/ul/li[1]/a"));
                return header.click();

        });
        this.Then(/^Counting the dropDown list of learn menu Header$/, function () {
                var learnList = element.all(by.xpath("//a[text()='Learn']/../ul//li"));
                expect(learnList.count()).to.eventually.equal(5);

                learnList.count().then(function (size) {
                        console.log(size);
                });

                for (var i = 0; i < learnList.length; i++) {
                        element.all(by.xpath("//a[text()='Learn']/../ul//li//a")).get(i).getText().then(function (text) {
                                console.log(text);
                        });
                }
        });

        this.Then(/^Enterting text in name box of The Basics tab$/, function () {
                element(by.model('yourName')).sendKeys("RamaSwamy");

                return element(by.css('.well .ng-binding')).getText().then(function (text) {
                        console.log(text)


                });

        });

};
