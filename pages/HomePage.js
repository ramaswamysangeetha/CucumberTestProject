"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class homePage {
    constructor() {
        // The below elements are of this site http://www.protractortest.org/#/
        this.toDoList = protractor_1.element(protractor_1.by.model('todoList.todoText'));
        this.valueAdd = protractor_1.element(protractor_1.by.css('[value="add"]'));
        this.toDoListCount = protractor_1.element.all(protractor_1.by.repeater('todo in todoList.todos'));
    }
}
exports.homePage = homePage;
;
