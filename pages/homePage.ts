import { $ } from "protractor";
import { browser, element, by } from 'protractor';


export class homePage {

    public toDoList: any;
    public valueAdd: any;
    public toDoListCount: any;


    constructor() {

        // The below elements are of this site http://www.protractortest.org/#/
        this.toDoList = element(by.model('todoList.todoText'));
        this.valueAdd = element(by.css('[value="add"]'));
        this.toDoListCount = element.all(by.repeater('todo in todoList.todos'));
    }
};