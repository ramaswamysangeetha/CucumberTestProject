"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const crew = require('serenity-js/lib/stage_crew');
exports.config = {
    chromeDriver: 'node_modules/protractor/node_modules/webdriver-manager/selenium/chromedriver_2.33',
    seleniumServerJar: './node_modules/protractor/node_modules/webdriver-manager/selenium/selenium-server-standalone-3.6.0.jar',
    //  seleniumAddress: "http://127.0.0.1:4444/wd/hub",
    //  SELENIUM_PROMISE_MANAGER: false,
    framework: "custom",
    frameworkPath: require.resolve('serenity-js'),
    serenity: {
        dialect: 'cucumber',
        stageCueTimeout: 30 * 1000,
        crew: [
            crew.serenityBDDReporter(),
            crew.photographer(),
            crew.consoleReporter()
        ]
    },
    specs: ['features/**/*.feature'],
    allScriptsTimeout: 50000,
    getPageTimeout: 50000,
    cucumberOpts: {
        require: ['features/**/*Page.ts'],
        format: "pretty",
        compiler: "ts:ts-node/register",
    },
    capabilities: {
        browserName: "chrome",
    },
    onPrepare: () => {
        protractor_1.browser.ignoreSynchronization = true;
        protractor_1.browser.manage().window().maximize();
        protractor_1.browser.get("https://angularjs.org/");
    },
};
