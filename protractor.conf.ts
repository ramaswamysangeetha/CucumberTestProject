import * as path from "path";
import { browser, Config } from "protractor";
const crew = require('serenity-js/lib/stage_crew');


export const config = {
    chromeDriver: 'node_modules/protractor/node_modules/webdriver-manager/selenium/chromedriver_2.33',
    seleniumServerJar: './node_modules/protractor/node_modules/webdriver-manager/selenium/selenium-server-standalone-3.6.0.jar',
        
  //  seleniumAddress: "http://127.0.0.1:4444/wd/hub",
  //  SELENIUM_PROMISE_MANAGER: false,

    framework: "custom",
    frameworkPath: require.resolve('serenity-js'),

    serenity: {
        dialect: 'cucumber',
        stageCueTimeout: 30 * 1000,
        crew: [
            crew.serenityBDDReporter(),
            crew.photographer(),
            crew.consoleReporter()
        ]
    },

    specs: ['features/**/*.feature'],
    allScriptsTimeout: 50000, //This is the overall Timeout
    getPageTimeout: 50000, //This is the Page timeout
    cucumberOpts: {
        require: ['features/**/*Page.ts'],
        format: "pretty",
        compiler: "ts:ts-node/register",
    },
    capabilities: {
        browserName: "chrome",
    },

    onPrepare: () => {
        browser.ignoreSynchronization = true;
        browser.manage().window().maximize();
        browser.get("https://angularjs.org/");
    },
};
